Nomad on Alpine Linux
=====================

This repository contains an Alpine Dockerized Nomad (Hashicorp), published to the public Docker Hub via automated build mechanism.

Rationale
---------
This image was initially designed to be used in a [Gitlab pipeline](http://docs.gitlab.com/ce/ci/pipelines.html), so instead
of installing Nomad in a Gitlab runner, the step in charge of deploying services uses a tiny (that's why Alpine) docker image
with Nomad pre-installed in it.

Usage
-----
You have two options for working with this image:
1 - Get the image from Docker Hub: `docker pull iflavoursbv/nomad-alpine`
2 - Directly build the image: `docker build -t iflavoursbv/nomad-alpine https://bitbucket.org/iflavours/nomad-alpine/raw/master/Dockerfile`

### General usage

Please notice this image is configured with a workdir `/scripts`, so make sure you mount such volume first.

**Scenario**: you have a Nomad script named as `sample-script.nomad` and a Nomad server running on `192.168.100.37:4646`

**Objective**: dry-run of the scheduler for determining what's going to happen when submitting the `sample-script.nomad` job.

```bash
$ docker run -v $(pwd):/scripts --rm iflavoursbv/nomad-alpine:latest plan --address=http://192.168.100.37:4646 sample-script.nomad 
```


Configuration
-------------
This docker image is based on the following stack:
- OS: Alpine Linux
- Nomad: latest=0.5.0 (see [tag list](https://registry.hub.docker.com/u/iflavoursbv/nomad-alpine/tags/) for more versions)


Dependencies
------------
- none

History
-------
* 0.2.0 - Upgrade Nomad to 0.5.6
* 0.1.2 - Upgrade Nomad to 0.5.0
* 0.1.1 - Upgrade Nomad from 0.4.0 to 0.4.1
* 0.1.0 - Initial version

License
-------

[Licensed under MIT License](https://bitbucket.org/iflavours/sbt-openjdk-8-alpine/raw/master/LICENSE)

